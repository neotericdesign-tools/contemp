# Contemp

Neoteric Design's technical blogging theme for Hugo.


## About the name

Contemp is named after Les Contemplations (The Contemplations), a collection of poetry by Victor Hugo, published in 1856. It consists of 156 poems in six books. Memory plays an important role in the collection, as Hugo was experimenting with the genre of autobiography in verse. The collection is equally an homage to his daughter Léopoldine Hugo, who drowned in the Seine.


## Run in production 

Hugo properly doesn't recognize the HUGO_ENV variable that Forestry and Netlify uses.  You can run in production with: 

`hugo server -e production` 

